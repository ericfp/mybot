# Web App Design Considerations #

## Compatability Target and Design Principles ##
* Ipad Chrome or Safari (whichever one is better)
* Android tablet Chrome
* Controls on right side of tablet easy to use with one hand
* Responsible design
* Open Source
* Abbreviations on display okay
* License (TBD)

## Types of Content ##
* controls for the robot (CTLS)
* settings (SETTINGS)
* * status information (STATUS)


## Note - Important ##
Additional features will be added in the future and may require the user interfac to change or be broken into multiple tabs or pages.


## CTLS ##
Must provide feedback that they were touched/clicked.

### Camera GoPro Hero 3###

### Ultrasonic Distance Sensor ###

### Other ###

### Settings ###
Settings can sometimes be seen as controls.  The distinction is that settings are changes less often then controls.

### Status ###
Status settings will be displayed from ajax calls and will be refreshed frequently.

### How it Works Under the Covers ###
* HTML5 
* Google Dart2JS v 1.3 or later
* Ajax calls to Microcontroller via ad-hoc wifi network
* RESTful interace on Microcontroller will be python and probably coded in "flask"
* Microcontroller will be resident on the robot and will be either a Raspberry Pi (RPi Model B) or BeagleBone Black (BBB) rev B or later

## Motor / Dirve Train / Locomotion ##
### CTLS ###
* Up arrow - go forward, repeat clicking increases speed
* Down arrow - go backward, repeat clicking increases speed
* Left Arrow, Right Arrow - turn in that direction (see Settings), hitting it more will change the turn bias
* Stop Start Button - Button will reflect curent state and will flip state when clicked (red for stopped, green for go), if go is hit the robot is turned on (Not sure what direction and how to do this but that UI doesn't matter)
* Reset wheel encoders (both at one time so one button)


### SETTINGS ###
* turn mode 1 - in place, i wheel back and 1 forward, forward turn with different forward speeds)
* motor speed (one speed), 0 (off) even if status is on to 100 (full)
* turn amount 0 to 100% (I have to figure this out)


### STATUS ###
* Left Motor Current Draw (0 to ~10 amps), 2 amps or more is critical (red)
* Right Motor Current Draw - see above
* Motor Controller Temperature (F)
* Left motor distances
* encoder ticks total since start
* encoder ticks total since last reset

